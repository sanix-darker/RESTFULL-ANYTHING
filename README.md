# RESTFULL-ALL

**REST (REpresentational State Transfer)** is an architectural style for developing web services. REST is popular due to its simplicity and the fact that it builds upon existing systems and features of the internet's HTTP in order to achieve its objectives, as opposed to creating new standards, frameworks and technologies.

This project is a collection of all potentials REST FULL API we can have on many languages, such as:
- PHP,
- Python,
- Java,
- Javascript,
- Go
- Ruby. 
- Haskell, etc... 

*_`NOTE:`_ At the begining, i wanted to work "from scratch", but, there is some framework and lib who are too lite and can seriously make the developpement of your RESTFULL API fast and simple, so in this project, i will only use lite tools.*

**>> This project is for BACKEND developpers!**